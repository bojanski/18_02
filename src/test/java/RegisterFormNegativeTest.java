import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RegisterFormNegativeTest {

    @BeforeMethod
    public void openTheBrowser() {
        Browser.openBrowser("chrome");
        Browser.openPage();

    }

    @Test
    public void regNegTest() {
        Browser.driver.findElement(By.xpath("//i[@class='fa fa-user']")).click();
        Browser.driver.findElement(By.xpath("//a[text()='Register']")).click();
        Browser.driver.findElement(By.xpath("//input[@value='Continue']")).click();
        String firstNameerr = Browser.driver.findElement(By.xpath("//input[@id='input-firstname']/following-sibling::div[@class='text-danger']")).getText();
        Assert.assertEquals(firstNameerr, "First Name must be between 1 and 32 characters!", "You are not registred");
        String lastNameerr = Browser.driver.findElement(By.xpath("//input[@id='input-lastname']/following-sibling::div[@class='text-danger']")).getText();
        Assert.assertEquals(lastNameerr, "Last Name must be between 1 and 32 characters!", "You are not registred");
        String emailErr = Browser.driver.findElement(By.xpath("//input[@id='input-email']/following-sibling::div[@class='text-danger']")).getText();
        Assert.assertEquals(emailErr, "E-Mail Address does not appear to be valid!", "You are not registred");
        String telephoneErr = Browser.driver.findElement(By.xpath("//input[@id='input-telephone']/following-sibling::div[@class='text-danger']")).getText();
        Assert.assertEquals(telephoneErr, "Telephone must be between 3 and 32 characters!", "You are not registred");
        String passErr = Browser.driver.findElement(By.xpath("//input[@id='input-password']/following-sibling::div[@class='text-danger']")).getText();
        Assert.assertEquals(passErr, "Password must be between 4 and 20 characters!", "You are not registred");
    }

    @AfterMethod
    public void tearDown() {
        Browser.driver.quit();
    }
}
