import net.bytebuddy.utility.RandomString;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginTest {


    @BeforeMethod
    public void openTheBrowser() {
        Browser.openBrowser("chrome");
        Browser.openPage();
    }

    @Test
    public void loginTest() throws InterruptedException {
        Browser.driver.findElement(By.xpath("//i[@class='fa fa-user']")).click();
        Browser.driver.findElement(By.xpath("//a[text()='Login']")).click();
        Browser.driver.findElement(By.id("input-email")).sendKeys("bob1hzz@gmail.com");
        Browser.driver.findElement(By.id("input-password")).sendKeys("12345678");
        Browser.driver.findElement(By.xpath("//input[@type='submit']")).click();

    }

    @AfterMethod
    public void tearDown() {
        Browser.driver.quit();
    }
}
