import net.bytebuddy.utility.RandomString;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RegisterFormPositiveTest {

    @BeforeMethod
    public void openTheBrowser() {
        Browser.openBrowser("chrome");
        Browser.openPage();

    }

    @Test
    public void regTest() throws InterruptedException {
        Browser.driver.findElement(By.xpath("//i[@class='fa fa-user']")).click();
        Browser.driver.findElement(By.xpath("//a[text()='Register']")).click();
        Browser.driver.findElement(By.id("input-firstname")).sendKeys("Boyan");
        Browser.driver.findElement(By.id("input-lastname")).sendKeys("Atanasov");
        Browser.driver.findElement(By.id("input-email")).sendKeys("bob1hzz" + RandomString.make(5) + "@gmail.com");
        Browser.driver.findElement(By.id("input-telephone")).sendKeys("+359893579778");
        Browser.driver.findElement(By.id("input-password")).sendKeys("12345678");
        Browser.driver.findElement(By.id("input-confirm")).sendKeys("12345678");
        WebElement subscriberadio = Browser.driver.findElement(By.xpath("//input[@type='radio'][@name='newsletter'][@value='1']"));
        subscriberadio.isSelected();
        if (!subscriberadio.isSelected()) {
            subscriberadio.click();
        }
        WebElement checkbox = Browser.driver.findElement(By.name("agree"));
        checkbox.isSelected();
        if (!checkbox.isSelected()) {
            checkbox.click();
            Browser.driver.findElement(By.xpath("//input[@value='Continue']")).click();
            WebElement firstrowtext = Browser.driver.findElement(By.xpath("//div[@id='content']//p[1]"));
            String text = firstrowtext.getText();
            Assert.assertEquals("Congratulations! Your new account has been successfully created!", text, "You are not registred!!!");
            Thread.sleep(5000);
        }
    }

    @AfterMethod
    public void tearDown() {
        Browser.driver.quit();
    }
}
